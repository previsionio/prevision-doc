# Prevision.io User Documentation Guidelines

This README gives some guidelines for writing documentation about Prevision Product. 

## Workflow

The documentation is part of the Prevision Features deliveries. Each new feature should be considered complete only if doc is available.

The Feature documentation process starts when the dev ends, about 2 weeks before putting new feature to production.


| Start of dev                  | Dev         | W-1 features available on integration server                   | W feature is on clud                    |
| ----------------------------- | ----------- | -------------------------------------------------------------- | --------------------------------------- |
| Start documenting new feature | Documenting | Doc available on https://previsionio.readthedocs.io/fr/rc-doc/ | new feature doc is on redthe doc latest |


Doc is started when features is kicked-off

## How to write doc

The doc is hosted on read the doc : https://previsionio.readthedocs.io/fr/latest/ and is publicy available.

Each time new doc is pushed on branch `master` it is compiled to be included into the latest documentation. Compilation parameter are available for admin of [prevision.io readthe doc account](https://readthedocs.org/projects/previsionio/).

The doc is written in [Restructured Text](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) ( except for this README that is the doc ... of the doc ) and its source code is hosted on bitbucket (here).


### Setup

In order to write doc, please respect this procedure 

```
git clone https://bitbucket.org/previsionio/prevision-doc.git
cd prevision-doc
git checkout dev
python -m venv py3-sphinx
source py3-sphinx/bin/activate
pip install sphinx
pip install sphinx-rtd-theme
make clean
make html
``` 

You then need some local http server to see the generated content. For example :

```
cd _build/html/
python3 -m http.server 
```

> go to http://localhost:8000/


Note : you may push to prevision documentation Repo only if you are contributor of the project.

### Git flow

Do not push on master. Open a branch for each new doc and merge into master once done :


```
cd prevision-doc
git checkout master
# Get last commit
git pull --rebase origin master
# Create a dedicated branch
git checkout -b feat/my-new-guide
# write and commit
git commit -am "feat: a message about what I did"
git push origin feat/my-new-guide
# Check 
make clean
make html
# Read your doc then merge
git pull --rebase origin feat/my-new-guide
git pull --rebase origin master
git push --force-with-lease origin feat/my-new-guide
git checkout master
git pull --rebase origin master
git merge --no-ff origin/feat/my-new-guide
git push origin master
# You doc will now be available to everybody in about 10mn
```

### Generate DocX

```
make singlehtml
cd _build/singlehtml/
pandoc -o index.docx index.html
```

### Styling the doc with CSS

In the `src/conf.py` file, add you css :

```
# -- Options for HTML output ----------------------------------------------
html_css_files = [
    'custom.css',
]
```

Then edit your `custom.css` file in `src/_static` 

## Documentation Good practices

### Split your theme

Split your concept as much as you can and respect genealogy of a concept ( "features" concept and "embedding" concept belong to "Dataset" for example).


### Document process, not UX element

Try to document the action allowed by Prevision Platform instead of document each button page
DO :

> In order to create an experiment, go the :doc:`/Studio/projects`  page , click on a project and click on "create an experiment" button.

DONT :

>  On the page `experiments/list` are a table of your experiments and button for creating :doc:`/Studio/experiment` 

### Check that all action available for a concept are documented  

For each and every Concept, you should have :

- A general presentation  of the concept
- its place in the Machine Learning Flow
- How to list and see all you ressources about this concept
- how to create one
- how to inspect and analyse one
- how to edit and remove

### Convention

Normally, there are no heading levels assigned to certain characters as the structure is determined from the succession of headings. However, this convention is used in Python’s Style Guide for documenting which you may follow:

\# with overline, for parts

\* with overline, for chapters

=, for sections

-, for subsections

^, for subsubsections

", for paragraphs

## Concepts

- Projects
  - Data
    - Dataset
    - Image
    - Datasource
    - Exporters
    - Connectors
  - Experiments
    - Models
    - Features
    - Predictions
    - Tasks
    - Versions
    - Report
  - Pipelines
    - Components
    - Templates
    - Runs
  - Deployments 
    - Experiments
      - Monitoring
        - Distribution
        - Drift
        - Usage
      - Predictions
      - Versions
    - Applications

