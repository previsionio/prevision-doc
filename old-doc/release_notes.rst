#############
Release notes
#############
*************
Version 10.21
*************

Studio
------

Features
********

- Hyper-parameters used during text similarity use cases are now displayed on models page
- DAG type operations are now displayed in different colors in order to visualise better the steps and results of a use case training
- Text Similarity use cases : 
    - Use cases can now be identified as deployable
    - Limitation of IVF-OPQ (at least 1000 unique ID from the configuration dataset) is now displayed
- Feature selection when setup use case properties is now possible using filter and mass selection
- For classification use cases, linear regression label on the interface is now replaced by logistic regression
- Out Of Memory errors are now identified in the DAG
- CATBOOST models are now available in the plateforme for following use cases types :
    - Binary classification
    - Multi classification
    - Regression
    - Timeseries
    - Images


Bug fixes
********

- Parsing issue with ZIP dataset using R notebook fixed
- Fixes of some google buttons
- New version of a use case keep configuration of the previous version
- Scatter Plot graph axes labels were inverted
- Fixes regarding preview DAG
- Models in performance chart are now displayed by descending performance score


Store
------

Features
********

- Text Similarity :
    - Deployment of Text similarity use cases is now possible
    - Single prediction are now available for deployed text similarity use cases
    - Strings of the prediction request are highlighted on the predictions table
    - Chart mean similarity score VS time is available in the « general » menu
- Deployable use cases dropdown is sort by model performance
- Display of the suppression modal for web app and notebook
- Shared and deployed use case are now editable by all concerned user instead of juste the creator of the use case

Bug fixes
********

- Fixe of an issue regarding the suppression of a deployed use case
- Selection of a GIT repository from the selection dropdown list in order to deploy an application is fixed
- Publisher and creator name of a deployed use case is now displayed
- All use case in the use case deployment dropdown are no displayed instead of only 25
- Fixe regarding the prediction probability round

*************
Version 10.20
*************

Studio
------

Features
********

- Pipelines :

  - you can now delete your pipeline components
  - Pipeline menu is no longer related to the data menu but get his own entry on the top bar

- Text similarity use case :

  - More configuration options of your train are now available
  - Predict duration is now calculated and displayed 
  - IVF-OPQ models for texte similarity trains is now available. Please not that this model need at least 1000 unique ID from your dataset in order to perform well

- Similarity scores are now available in predictions files
- 2nd step of use case training is now called « Configure use case » instead of « Configure dataset »


Bug fixes
********
- « selected feature » is now changed for « selected feature engineerings » on a model page
- Design of the table of metric comparison for text similarity use cases is now clean
- Auto report for Time series use cases is now available
- f(response time) = performance graphique is now available even if only one model was trained
- Star system metric is now more reliable
- fixes regarding DAG preview during use case configuration


Store
------

Bug fixes
********
- Public use case deployment is now possible
- Bug on prediction page of a deployed model, editable fields are now available

*************
Version 10.19
*************

Studio
------

Features
********

- Texte similarity train : you can now train text similarity use cases using a catalog dataset and an optional queries dataset for validation score calculation.
Following models can be trained :
	- Brute force
	- Cluster pruning 
	- LSH
	- HKM
- Text similarity models : Once trained, the detailed of the models can be analyzed in the detailed page of the use case
- Text similarity bulk predict : You can also use the bulk predict functionality in order to get prediction of most accurate answer to a queries files regarding the k results
- Dataset analyses : you can now start the analyse of your datasets by clicking on the icon on dataset list and get the results the same way once the compute is done
- Train profil : the profil (quick/normal/advanced) is now display on the use case page
- Use case analysis : new graph allowing to compare the performance VS response time for each trained models of a use case allow you now to choose the better model to deploy regarding your objectives

.. image:: https://storage.cloud.google.com/prevision-doc/graph%20accuracy%20VS%20prediction%20time%20V2.png
   :align: center

- Object detector label became object detection
- Action buttons on table displayed now a specific cursor

Bug Fixes
********

-  Preview DAG when configure your train is now dynamic regarding the use case profil selected
- Times series selection model display no longer models not available for time series use cases
- Use case train is no longer blocked when target column is named « Target »
- Auto refresh of use case list after delete a use case

Store
------


Bug Fixes
********

- Predictions on use cases type regression are fixed

*************
Version 10.18
*************

Studio
------

Features
********

- Tests end-to-end 
- New graphic on use case page f(Time) = metric allowing users to choose best model for his application regarding prediction response time regarding performance metric
- Preview DAG : when building your use case you can now view the prevision.io DAG that will be run during the training regarding the parameters you want to apply

Bug Fixes
********

- Feature grade indicators in a use case are now relevant
- Star system for MAPE metric is now fixe
- models details are now accessible from DAG

Store
------

Features
********

- Tests end-to-end 
- Object detector : you can now choose the deployed model

Bug Fixes
********

- App deployment blocked if mandatory options not completed


*************
Version 10.17
*************

Studio
------

Features
********

- Dataset configuration : weight type is forced to numerical only
- Data transformation pipelines V0

Bug Fixes
********

- Dataset names replaced by dataset Ids
- Notebook ressources are now aligned with administration choices
- Import dataset during creation of use case is now possible

*************
Version 10.16
*************


Studio
------

Features
********

- Increase time of validation email : validation email for a cloud.prevision.io subscription is now increase from 5 minutes to 30 minutes
- NLP : Use case creation is blocked if user unchecked all textual features from feature engineering with a 100% textual features dataset
- Some liste sort were not functional and have been deactivated in order to prevent deviant behavior. These sorts will be re-activated as soon as the identified technical issue will be corrected

Bug Fixes
********

- Unification of date format between store and studio
- Changement of data type and training during the creation of a use case is no longer impossible
- All feature engineering used during the creation of a use case are now displayed in the features configuration menu of a use case
- Numerical features using thousands separators are no longer considered as categorial features
- Token validity time as been increased
- Object detector :

  - Labels and percentages of an object detector use case are now visible in the image sample of the use case
  - Prediction images from a shared use case are now visible
    
- Dashboards lists are now refreshed when user delete a use case or a dataset from corresponding menus
- Decision tree’s numerical values are now given to two decimal points
- Bivariate analysis graph is now sort by lexicographic order
- Conflict between null/negative values and metric calculation

Store
------

Features
********

- Drift visualisation between main and challenger models is now available in order to choose better the production model

Bug fixes
********

- Feature distribution graph are now fixed
- Main & challenger models :

  - Improvements of prediction and feature distribution graphs
  - Improvement of drift graph
  - General improvement of graphs design 
    
- No results from a prediction
- Unification of date format
- Card images of applications are now displayed
- Most probable predictions from a multi classification application is now displayed first


*************
Version 10.15
*************


Studio
------


Features
********

NLP : 

- New rules regarding NLP use case have been defined regarding the dataset:

  - A 100% textual feature dataset : 
    
    - Simple and normal models have been deactivated in the use case creation interface. Only the Naive Bayes (NBC) model and advanced models can now be applied. This is due to the fact that simple and normal models are not working with dataset containing only textual features dataset and produce errors in the DAG.
    - Messages in the interface are now visible in order to explain to the user why these models are not available in those cases

  - A 0% textual feature dataset :

    - Feature Engineering « textual features » are deactivated as they cannot be used in those cases
    - A message in the interface explains to the users the new interface behavior

  - Mix between textual and other features :
    
    - New messages have been displayed in the interface explaining that FE textual features will be only applied to advanced models and Naive Bayes model

- The default configuration of a use case including textual features is now as the image bellow. Users will now used a more powerful textual feature by default

.. image:: https://storage.cloud.google.com/prevision-doc/Training%20options1.png
   :align: center

Experimental time series : 

- This option is no longer available in the interface due to frequent problems. Will be reintegrated after mre research.
- The SDK no longer allows you to use this option.

Bug fixes : 

- Shared memory limit increased from 8Gb to 64Gb
- Duplication of dataset features


Store
------

Features
********

- Model challenger :

  - Tag main & challenger : you can define for each deployed use case which model is your main and, optionally, which one is the challenger. You will then access to comparison graphs between these two models and change at any time the main model in production.
  - Versioning : each new configuration of main/challenger model creates a new version of your application. For each version, you now have access to metrics allowing you to determine which model is the most suitable to be deployed between your main and your challenger models.
  - Rollback : you can rollback to an old main/challenger model configuration by clicking on the rollback button in the version menu of a deployed application

- New graphs for a deployed use case :

  - In the usage menu, you will now find graphs showing you the numbers of predictions, the response time, all the errors in the last 24 hours and all the errors since deployment.
  - Prediction distribution :
  
    - Binary classification : positive and negative prediction distribution. Raw for the train model, validation threshold for the production model
    - Multi-classification : repartition of predictions by modality. Raw for the train model and best prediction for production model. The number of displayed top modalities can be configured.
    - Regression : Distribution of values for train and production predictions models split by intervals equal of 1/10 of train results intervals

.. image:: https://storage.cloud.google.com/prevision-doc/Prediction%20distribution.png
   :align: center

- Shared use case : You can now deploy a model from a shared use case
- Activity logs of deployed application are now available in the application.



*************
Version 10.14
*************


Studio
------

Features
********

- Object detector :

  - Migration from YoloV3 to YoloV5 and improvements of object detector uses cases
  - Target file can now be load for object detector prediction in order to have a prediction score
- Time series :

  - Rules of time series are now displayed in the interface of use case creation
  - Derivation and forecast windows fields are now easier to fulfill thanks to auto completion and restriction according to time series rules
- Administration :

  - CPU, RAM and notebook lifetime can now be defined up by admins
- Versioning :

  - Models can now be identified as deployable. In the store, the list of deployable models is now the one identified in the studio as deployable
- Reporting :

  - Report of use case models can be now generated trough the use case page
- Feature importance :

  - Feature importance of a model are now paged 10 by page
  - You can also search a feature and sort the features by importance
- DAG :

  - The DAG of a use case is now more detailed
- Other features :

  - When new version of a use case is created, the type of training is no longer editable
  - Feature grades have now an explanation in the interface
  - In the model page, all feature engineering used by this models are now displayed, even the internal ones
- Bug fixes :

  - Difference between top bar and use case page about best performance calculation
  - Bug regarding object detector use cases and the « filename » name column
  - Train a classification using error_rate_binary metric created an error

Store
-----

Features
********
- Object detector :

  - Object detector use cases can now be deploy from the studio to the store

- Other features :

  - List of deployable models is now limited to identified models in the studio
  - Private GIT repositories are now identified with a lock icon
- Bug fixes :

  - Status display not « running » when a use case is deployed
  - Fields of a new app versioning where not pre filled when using GitHub

*************
Version 10.13
*************

Release date: 2020-10-22

Studio
------

Features
********

- User interface of models selection : Users choices regarding model selection in advanced options during the creation of a use case are now extended.
- FastText : First NLP treatments are coming in Prevision.io platform. Text roles features of a use case can now be optimized using NLP algorithm
- Image Detector use cases : migration to YoloV5 treatment allowing a better treatment for image detector use cases
- UX/UI improvements


Bug fixes
*********

- Fix of a display issue regarding number format depending on user interface language
- Lack of some calculated feature grades
- F1 score calculation of a model is now based on the optimal threshold value
- Fix of graphical display regarding bivariate analysis

Store
-----

Features
********

- Webapp and notebook versioning : creation of new version of an already existing web app and notebook
- Access to private GitLab repo : SSO connexion to GitLab
- Build and deploy logs are now available for webapps
- application list of allowed user is now editable


Bug fixes
*********

- Some field were not typed producing errors when fulfill with wrong type of data
- fix regarding an issue affecting all user of an instance instead of the user selected

*************
Version 10.12
*************

Release date: 2020-10-08

* Feature engineering importance
* Deployed Model SDK


*************
Version 10.10
*************

Release date: 2020-09-11

* Free trial version
* New interface for deployed experiments

************
Version 10.9
************

Release date: 2020-08-20

* New Prevision.io Studio homepage
* New Prevision.io Studio help page


************
Version 10.8
************

Release date: 2020-08-06

* SDK: Add versioning & sharing methods
* Add embedded support in store and studio through Zendesk

************
Version 10.7
************

Release date: 2020-07-23

* Connectors for Google Cloud Platform Buckets and BigQuery
* Advanced analytics for time series datasets
* Public mode for app deployment in store
* Subdomain URL mode for app deployment in store
* Add the capability to define environment variables when deploying apps in stores


************
Version 10.6
************

Release date: 2020-07-09

* Various improvements for apps deployment in store
* Better handling of very large datasets (> 10k columns)


************
Version 10.5
************

Release date: 2020-06-25

* Optimizations & bug fixes
* Model and app deployment is now entirely located in the Prevision.io store

************
Version 10.4
************

Release date: 2020-06-11

* Detailed statistics and analyses for datasets accessible from the data page

************
Version 10.3
************

Release date: 2020-05-28

* Usecase versioning


************
Version 10.1
************

Release date: 2020-04-10

* Create a new experiment from an existing one
* Simple models updated in order to match classical model analytics
* R & Python packages updated + new packages availlable for development environment

************
Version 10.0
************

Release date: 2020-03-05

* New graph-based experiment training monitoring
* Update scheduler page

***********
Version 9.7
***********

Release date: 2020-02-20

* Update notebook page to include current CPU & RAM usage
* Update and relocate administration page (now in top-right menu)
* Access data explorer from data screen

***********
Version 9.6
***********

Release date: 2020-02-06

* Change and relocate main menu to top bar
* Start experiment from data screen
* Update contact page

***********
Version 9.5
***********

Release date: 2019-12-19

* Refactoring of the main dashboard screen
* Refactoring of the experiment screen, including new analytics
* R & Python packages updated to matchs experiment APIs
* Improved explain screen stability when simulating predictions
* Added support of object detection experiment with CPU only (might take some computing time)
* Feature quality estimation

***********
Version 9.4
***********

Release date: 2019-10-04

* Refactoring of the data screen
* R & Python packages updated to matchs data screen APIs
* Improved rules of detection of typical columns (ID, TARGET, FOLD, WEIGHT)
* Improved explain screen stability when values are missing
* Improved date columns parsing in a dataset that handles multiple time zones
* Faster prediction time retrival when listing a high number of predictions
* Creation of an open data base with accessible data for special days (holidays, public days, sales, ...) and for weather data

***********
Version 9.3
***********

Release date: 2019-08-14

* Refactoring of the new use case screen
* R & Python packages updated to matchs new use case APIs
* Refactoring of Prevision.io APIs. Documentation available @ https://xxx.prevision.io/api/documentation (xxx = instance name)
* Creation of instance specific Prevision.io's store, visible @ https://xxx.prevision.io/store (xxx = instance name)
* Optimisation of training time for gradient boosting trees models