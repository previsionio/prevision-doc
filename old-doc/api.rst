###
Api
###

Prevision.io offers the possibility of training, predicting and use SDK by API call.

All the APIs available to interact with Prevision.io, without front-end, are detailed below:
https://xxx.prevision.io/api/documentation (xxx is the name of your instance).

**********
Master key
**********

Any HTTP request for access to the prevision.io APIs must include a Header Authorization whose value is a
valid JSON Web Token.
This master token must also be set to use the SDK, either through an environment variable or explicitly when
initializing the client.

This can be obtained by going to the user menu and clicking on the `API key` item
(`Administration & API key` if you have admin rights):

.. image:: _static/images/api_menu.png
   :align: center

The component to copy and (if needed) regenerate the master token is located on the right of the screen.

.. image:: _static/images/master_token.png
   :align: center

Warning: This key is strictly private. It must therefore be known only to the account holder.

If the key is ever compromised, it can be regenerated. In this case the old key will become obsolete.
It will then be necessary to update your applications with the new key.
