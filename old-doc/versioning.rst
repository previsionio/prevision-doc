##################
Usecase versioning
##################


How to keep track of your experiments?
--------------------------------------


Creating versions
=================

From version 10.1 onwards, it is possible to create multiple versions of a experiment, preserving settings while
possibly modifying them.

This allows to easily try different settings, approaches and datasets for a given problem, and keep track of
the results of the experiments in a single location.

You can create a new version of a experiment from three different locations :

- the main experiment listing: click on the ``...`` on the experiment for which you want to create a new version,
then ``New version``:

.. image:: _static/images/new_experiment_version.png
   :align: center


- the experiment "general" page: on the bottom, the "New version" will take you to the experiment creation screen:

.. image:: _static/images/new_version_general.png
   :align: center


- the "versions" page: here you will see all versions of a experiment and you will also be able to create a new one:

.. image:: _static/images/new_version_infos_versions.png
   :align: center


When a new version is created, you will be taken to the experiment creation screen where all settings will be exactly
as you set them up for the first experiment.

You can modify all of the settings : problem type, dataset, model settings, etc, although it is necessary to keep in
mind that some previous might not be applicable anymore once you change the problem type (as an example, if the
first experiment was an image classification experiment, changing the problem type to a tabular classification will
mean that the "image folder" setting will not be applicable). It is up to you to keep your experiments consistent or
to create a new experiment when it is needed.

If your experiments deviate too far from the original goal, you can create a completely new experiment while still
preserving some settings by using the "duplicate experiment" button on a experiment "general" tab :


.. image:: _static/images/duplicate_experiment.png
   :align: center

Doing will take you to the experiment creation screen, but the new created experiment will be independent of the previous one,
resetting the version history.


Versioning info
===============

From every version of a experiment, you can access the listing of all the versions that were created in the "versions" tab.


.. image:: _static/images/listing_versions.png
   :align: center


Here you can compare versions scores and create new versions from each of the existing iterations.

.. note::
    When you create a new version from an existing version, the setting from this version will be applied in the experiment
    creation screen. However, the version number will always be set to increment the highest existing version number.
    For example, on a experiment with 4 existing versions, you want to create a new version using version nº3 (because,
    for example, the 4th experiment was a mistake). The new version, even if created from version nº3, will be nº5.
