#########
Notebooks
#########

Prevision.io offers various tools to enable data science use cases to be carried out. Among these tools, there are notebooks and production tools.

These are accessible by clicking on the tab below:

.. image:: _static/images/top_bar_notebooks.png
   :align: center

You will then be directed to the following screen:

.. image:: https://storage.cloud.google.com/prevision-doc/Notebook%20screen.png
   :align: center

***********************
Development environment
***********************

JUPYTERLAB
==========

For Python users, a JUPYTERLAB environment (https://github.com/jupyterlab/jupyterlab) is available in Prevision.io

Note that a set of packages is preinstalled on your instance (list: https://previsionio.readthedocs.io/fr/latest/_static/ressources/packages_python.txt), particularly the previsionio package that encapsulates the functions using the tool's native APIs.
Package documentation link: https://prevision-python.readthedocs.io/en/latest/

R STUDIO
========

For R users, a R STUDIO environment (https://www.rstudio.com) is available in Prevision.io

Note that a set of packages is preinstalled on your instance (list: https://previsionio.readthedocs.io/fr/latest/_static/ressources/packages_R.txt), particularly the previsionio package that encapsulates the functions that use the tool's native APIs.
Package documentation link: https://previsionio.readthedocs.io/fr/latest/_static/ressources/previsionio.pdf