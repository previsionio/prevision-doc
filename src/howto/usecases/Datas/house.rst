*********************
House pricing Dataset
*********************

Motivations
===========

The House pricing dataset is a typical regression dataset where you try to guess the sell price of an house.

Description
===========


Downloads
=========

You can download the dataset `here <https://storage.googleapis.com/pio-tabular/regression_house_80.csv>`_ ( and validate with `the testset <https://storage.googleapis.com/pio-tabular/regression_house_20.csv>`_ )

Features
========

There are 20 features. Each row is an house transaction.

Target
======

target is named `TARGET` but is the sell price of each transaction.