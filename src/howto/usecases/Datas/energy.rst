**************
Energy Dataset
**************

Motivations
===========

The energy datast is a timeserie dataset about energy consumption with custom lag columns and a fold colun for good cross validation.

Downloads
=========

You can download the dataset `here <https://storage.googleapis.com/pio-tabular/elec_train.csv>`_ . An holdout dataset can be downloaded `here <https://storage.googleapis.com/pio-tabular/elec_valid.csv>`_

Features
========

There are **3** features : a datetime, the Temperature and a flag telling if it's public holiday or not. There 2 features built on target with lag of 1 day ( same time, yesterday and same time 7 days ago )


The fold column is properly built fold by time block for cross validating the result.

Target
======

Column named **Target** is the Target. This is the volume of energy consumed in 30mn.

