*************
Churn Dataset
*************

Motivations
===========

The Churn Dataset is a typical B2B dataset where each row is a customer's events summary for a period and one of the columns is a important event from a business point of view : churn, buying of a more expensive plan (up sell ) or buying of another product ( cross sell ). This is a **binary classification** problem on a :doc:`tabular dataset </studio/experiments/automl/tabular-data>`


Downloads
=========

You can download the dataset `here <https://storage.googleapis.com/pio-tabular/churn.csv>`_

Features
========

There are **19** features. All of them are informations about customer at the date when the dataset was extracted. CustomerID is the index of the dataset.

Target
======

Column named **Churn** is the Target. It is a flag telling if the customer has churned the week the dataset was generated. Its rate is about 26,6%

