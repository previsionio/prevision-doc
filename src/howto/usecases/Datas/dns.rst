*******************
DNS attacks Dataset
*******************

Motivations
===========

The DNS attacks Dataset is interesting dataset about Cyber-attack. Each row is a communication event with feature about it ( port, Ip src, ... ) and the target is a Categorical feature representing a cyber-attack group. There are a lot of features and rows. This is a **Multi-classification** model on :doc:`tabular dataset </studio/experiments/automl/tabular-data>`

Downloads
=========

You can download the dataset `here <https://storage.googleapis.com/pio-tabular/dns_attacks.csv>`_

Features
========

There are **84** features. 

Target
======

Column named **Class** is the Target. This is a Categorical target with 6 modalities.
