#######################################################
Model selection, how to chose the one that fit my need?
#######################################################

AutoML has the great advantage to try and build a lot of models in a row. Yet, in most of case only one of them is :doc:`going to Production </studio/deployments/index>`.

This guide show you how to evaluate and select your models according to business objectives ( as `there should not be any project without defined R.O.I. <https://content.prevision.io/can-an-ai-project-succeed-without-delivering-any-roi>`_ ).

Its advices are fine for any project but you could try to see it by your self with one of :doc:`our public standard Dataset </howto/useful_datas>` and a `Prevision.io account <https://cloud.prevision.io>`_ 

Purpose of Datascience project
==============================

Datascience has left the Labs and is now a useful tools to improve industrial process or prevent loss. Yet it sometimes struggle to deliver return of investment.

As most of the Datascience process and tasks are not streamlined thans to many tools and software, it's the Datascientist's duty to chec that datascience projet solve business problem.

Datascience is a set of tools to build predictive models. A large amount of them fall in two categories :

- classification : you got some datas or description of something and the models tells you whic catagory it belongs to
- regression : you got datas and your models tells you in how many days something will break or how many of your items you are going to sell, etc.

First and foremost, any model will make some error. Evaluating them is knowing how much this errors will cost and compare it to the gain you get from using this model. If using a model makes you earn 1M€ but its errors cost 30 000€, you should probably go for it.

As obvious as it sounds, if your model's errors cost you 500 000€ to earn 20 000€, you 'd better do not using it.

The cost of an error comes mostly from two things :

- you miss an opportunity and loose money. 
- you take an action that cost more than if you had not done anything

This should put in regards to the gain from a model that comes from :

- avoiding a loss
- getting more gain


..  admonition:: That is the action that costs money
    
    So evaluating a model should always be done keeping in mind that **models' predictions trigger actions that cost money that must prevent loss or earn gain**. If you do nothing from your model's predictions, do not bother evaluating it as its errors won't cost nothing to your company ( because you don't use it ).

Thus, evaluating a model comes down to answering this question :

- how many times will the model make an error on my production data?
- how much errors would I have done without the models or with a random strategy?
- how much cost each type or error?


Example of missed opportunities 
-------------------------------

Here are some standard missed opportunities due to model error

.. csv-table:: Missed opportunities
   :file: data/missed_opportunities.csv
   :header-rows: 1



Example of unnecessary expenses
-------------------------------

And here are some errors that make you take action 


.. csv-table:: Error cost from unnecessary action
   :file: data/expensive_action.csv
   :header-rows: 1



Some terms
==========

False Positive
--------------

On a classification, the models tells you something will happends but it does not. As a consequence you take some action that are unnecessary

False Negative
--------------

The model miss a prediction and does not alert you. You do not take action and have some loss for not taking action

Under estimation
----------------

The model forecasts less quantities or a shorter time to an event. 

Overestimation
--------------

The models forecasts too much.

Lift
----

Lift is a very important concept that any datascientist must understand.

Lift is "how much better is your model than a random decision" and this evaluation is often neglected. Let's say for example you got some transaction, a credit card transaction or a sales on shopping website. Your past 7 years datas tels you that one transaction out of 10 ( 10% ) is a scam.

You the decide to block 1000 transactions out of 100 000 with 2 methods :

- Set A : block 1000 random transactions pick at random
- Set B : block 1000 transactions where your models told you "Fraud: True"


In your set A, in average you are getting 100 fraudulous transaction, as the averate rate of fraud is 10% and you pick 1000 transactions randomly

In your set B, if you model is perfect ( which never happens ) you should get 1000 fraudulous transactions as all the transaction tagged fraudulous by your model are indeed fraudulous. If your model is not perfect , you are maybe getting 980 true fraudulous transaction


The ratio of target in your model's selection upon the ratio of target in a random selection is called "lift"


Precision
---------

Recall
------
