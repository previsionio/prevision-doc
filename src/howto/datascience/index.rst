#################
Datascience Guide
#################


.. toctree::
    :maxdepth: 2


    exploring_data
    release_a_model
    cross-validation
    text-classification    
    synthetic-data