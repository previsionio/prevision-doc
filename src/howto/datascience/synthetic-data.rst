##############################################
Synthetic Data for machine Learning :  A guide
##############################################

.. meta::
    :description lang=en:
        In this guide learn what are synthetic data, how to generate them for datasience and machine learning 
        project, and standard method to learn them


As often quoted, "Data is the new oil" and we, datascientist, clearly know the value of good data to train our model. However, we often face some problems with datas :

- we don't have access them because of confidentiality issues
- we do not have enough
- they are unbalanced and thus lead to biased model toward more represented class.

In this guide, we discuss the usage and performance of synthetic data generation. If you want to try it by yourself you can :

- Grab the dataset in :doc:`our datapack section </howto/usecases/Datas/house>`
- generate Fake data  with `our synthetic data generation datascience service  <https://sdg.cloud.prevision.io/>`_


Usage of synthetic Data
=======================

A common problem in B2C industry, telecom, bank,... And healthcare is confidentiality of data. We want to build models upon some dataset but want to avoid showing any information about personal record.

For example, you may have a dataset whom each rows is age, gender, size, weight, place of residence,... Even if data are anonymized by removing name or email, it's often easy to find some particular person which unique combination of features, for example :

   A 30 years old man who live near Gare du nord, earn 30 000€ a month , whom wife is engineer and had subscribed to sport magazine

would be unique enough to know a lot about him, even without identity data. So for some industry, exposing suche sensitive data, even for modeling, is clearly a no go.

Second problem is lack of data or worst, lack of data for some segment of the population that will then be disadvantaged by models. 

For example, let's say that we got a dataset with 10000 men and 1000 womens and we are using RMSE as Metric for our training. Let's say for sake of this article that every prediction for man have the same error :math:`manError` and womens have a :math:`womanError` constant error

The Score of a model will be :

.. math::

   \sqrt{\frac{10000 * manError^2 + 1000*womanError^2}{11000}}
   

.. code-block:: python

   df['RMSE'] = ((10000*(df['manError']**2) + 1000*(df['womanError']**2))/(10000 + 1000))**.5


And the errors of each gender will weight as follows on the training metric :

.. csv-table:: Gender error weight
   :file: data/ht.csv
   :header-rows: 1


We clearly see that making an error on woman segment weights much less on the optimized metric, as going from a :math:`100` to  :math:`10000` woman error only goes from :math:`100` to :math:`3016` total error yet for man it goes from :math:`100` to :math:`9534`.

As goal of machine learning model are to find minimum of givne metrics, a model trained on this data will advantage man Segment over woman Segment. 
Of course, if we had the same number of mens datas and womens datas, the error will be balanced and no bias toward some segment will happen. But adding more woman datas is not enough, we cannot just copy and paste more rows, as it will lack of diversity. 

What we should do instead is adding "realistic" woman Datas by sampling from the underlying distribution of each woman feature


.. figure:: img/synthetic/woman_gaussian.png
   :align: center
   :alt: woman weight distribution
   :scale: 80%

   Weight distribution for some segment 


Yet, if we just do that the naive way, this could happen :

.. csv-table:: A random sample from latent distribution
   :file: data/randomwoman.csv
   :header-rows: 1

(*Note: of course retired women may play american football yet weighting 12kg and playing american football when you are 82 years old is quite odd*)


There are too tarpit when generating fake data :

- Assuming everything is a normal distribution and reducing a feature to its average and its deviation. In fact, most of real world distribution are skewed
- not caring about `joint probability distribution. <https://en.wikipedia.org/wiki/Joint_probability_distribution>`_

And this is were good synthetic Data Generator come to the rescue.


Performance of synthetic Data Generator evaluation
==================================================

Like every datascience modelisation, synthetic data generation needs some metric to measure performance of different algorithm. This metrics should capture three expected output of synthetic data :

- distribution should "looks like" real data
- joint distribution too ( of course, someone who weight 12kj should not be 1,93m tall ) ( *likelihood fitness* )
- Modelisation performance should stay the same ( *machine learning efficiency* )

Statistical properties of Generated Synthetic Data 
==================================================

First obvious way to generate synthetic data is assume every feature follows a normal distribution, compute means and deviation and generate data from gaussian distribution ( or discrete uniform distribution ) with the following statistics :

.. csv-table:: Dataset statistics
   :file: data/stats.csv
   :header-rows: 1



In python 

.. code-block:: python

   [...]
   nrm = pd.DataFrame()

   for feat in ["price","sqft_living","sqft_lot","yr_built"]:
      stats=src.describe()[feat].astype(int)
      mu, sigma = stats['mean'], stats['std']
      s = np.random.normal(mu, sigma, len(dst))
      nrm[feat] = s.astype(int)

   # For discrete Features
   for feat in ["bedrooms","bathrooms"]:
      p_bath = (src.groupby(feat).count()/len(src))["price"]
      b=np.random.choice(p_bath.index, p=p_bath.values,size=len(dst))
      nrm[feat] = b


Yet this lead to bad feature distribution and joint distribution.

In the following plot, we show the distribution of features of :doc:`a dataset </howto/usecases/Datas/house>` (*blue*) , distribution of a synthetic dataset generated with a CTGAN in quick mode (*orange*, only 100 epoch for fitting ) and distribution of data assuming each feature is independant and follows a normal distrbution ( see code above )

For prices, we see that generated data perfectly captured the distribution of it, so  good that we barely see the orange curve. Gaussian  random data has same mean and deviation than original data but clearly does not fit the true data distribution. For yr_built, the synthetic data generator (*orange*)  struggles to perfectly capture the true distribution (*blue*) but it looks better than the average


.. figure:: img/synthetic/pairplot_1.png
   :align: center
   :alt: Pairwise features distribution
   :scale: 60%

   prices and bedrooms distribution


Here below is same density chart for prices, zoomed

.. figure:: img/synthetic/compare_distrib.png
   :align: center
   :alt: Compare distribution
   :scale: 60%

   Zoom on price distribution



It's more difficult to see joint distribution on this chart but we can draw contourmap to get a better view :



.. figure:: img/synthetic/true_data_joint_distrib.png
   :align: center
   :alt:  Original true Data Joint distribution
   :scale: 60%

   Original true Data Joint distribution of prices and year constructed


.. figure:: img/synthetic/generated_data_joint_distrib.png
   :align: center
   :alt:  Generated Data Joint distribution
   :scale: 60%

   Generated Data Joint distribution of same features. We see density start to reach those of real data


.. figure:: img/synthetic/independant_var_joint_distrib.png
   :align: center
   :alt:  Independant random variable distribution
   :scale: 60%

   Independant random variable distribution : jointplot density is totally wrong



The same conclusion goes for others features too. Random independant data do not fit distribution and joint distribution as good as properly generated data :

.. figure:: img/synthetic/pairplot_2.png
   :align: center
   :alt: Pairwise features distribution
   :scale: 60%

   each feature has its own distribution and distribution relative to others


Of course, better models than simple Normal distribution exist. You can build kernel density estimation, gaussian mixture model or  use other distribution thant a normal one yet doing so mean you start to use more and more complex model, till you 'll discover that going for Generative Adversarial Network is the way to go.


Generating good Synthetic Data
==============================

Like image before, Tabular data benefits from usage of Generative Adversarial Networks with Discriminator. In order to avoid the issue shown above, we can train a neural network on data whom input would be  for each sampe a vector of :

- discrete probability for each mode of categorical variable
- probability of each mode of a a gaussian mixture model 

The network is then tasked to generate a "fake sample" output and a Discriminator then try to guess if this output comes from the Generator or from the the true datas.

By doing this, the Generor will learn both distribution and joint distribution between each feature, converging to a generator that output sample "as true as the real one".

The current best implementation of this idea is `CTGAN <https://github.com/sdv-dev/CTGAN>`_ that you can `use on our Marketplace <https://sdg.cloud.prevision.io/>`_ 


Machine Learning efficiency
===========================

Of course, the best practical performance estimation of generated data is Machine Learning efficiency. We ran some performance test on `our automl platform <https://cloud.prevision.io>`_  with the optimal parameters ( all models tried, hyperoptimisation and Blending of model ) on 3 datasets with the same target :

- Original dataset
- Synthetic dataset fitt on 800 epochs
- random Dataset with no joint probability 

We expect that by using automl platform, only data quality would change the performance.

Here are the results :

.. figure:: img/synthetic/res_true.png
   :align: center
   :alt: Modelisation on true Data
   :scale: 100%

   Modelisation on true Data 



.. figure:: img/synthetic/res_fake.png
   :align: center
   :alt:    Modelisation on Synthetic Data 
   :scale: 100%

   Modelisation on Synthetic Data 



.. figure:: img/synthetic/res_randm.png
   :align: center
   :alt: Modelisation on random independant data
   :scale: 100%

   Modelisation on random independant data


We see that with CTGAN Synthetic Data, Machine Learning efficiency is preserved, which is not the case for independant data distribution. Moreover, this efficiency is good even for sgemnt of the original dataset, meaning you can generate synthetic samples for under represented category and keep their signal high enough to fix bias in your dataset.


Going Further
=============


You can read `the original paper for CTGAN <https://arxiv.org/abs/1907.00503>`_  and get it `here  <https://github.com/sdv-dev/CTGAN>`_. 

The CTGAN implementation is available on `our Marketplace <https://sdg.cloud.prevision.io/>`_ and you can test it with one of :doc:`our public standard Dataset </howto/useful_datas>`
 
Remember that synthetic datas is very useful when you need to enforce data privacy or unbiase dataset. Training a Synthetic Data Generator for few hours build a generator that can be used to generate data in a few seconds and thus synthetic data should become part of your datascientist toolset too.
 