#########################################################
A list of some useful Dataset to explore Machine Learning
#########################################################


Tabular Dataset
===============


.. csv-table:: List of dataset
   :file: datasets/list.csv
   :header-rows: 1


Images
======

.. csv-table:: Images Folders
   :file: datasets/images.csv
   :header-rows: 1


NLP
======

.. csv-table:: NLP Folders
   :file: datasets/nlp.csv
   :header-rows: 1


Externals models
======

.. csv-table:: Externals Models
   :file: datasets/models.csv
   :header-rows: 1
