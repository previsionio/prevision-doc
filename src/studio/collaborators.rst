Contributors
============

By clicking on contributors on the main menu of a project, you will access the list of all users of the project.
If you have sufficient rights ("Admin") on this project, you will be able to add & delete users from the project and modify
users' rights.


Roles & rules
^^^^^^^^^^^^^


* Viewer : you can access to all pages (except project settings) with no possibility of creation or edition
* Contributor : viewer rights + you can edit and create resources inside the project
* Admin : contributor rights + you can manage users and modify project properties


Add & delete
^^^^^^^^^^^^

If you are admin in a project, you can manage users into your project.

To add a user, you have to enter the collaborator email into the top left field,
set his right using the dropdown menu and click on the “invite this collaborator”.
Please note that you can only invite collaborators that already have a prevision.io account.

To change the rights of a user into the project, you just have to select the new role using the dropdown.
To be sure that the project and users properties can be managed, at least one collaborator have to be admin of the project.

To remove a collaborator from the project, use the trash button on the left side of the list.


.. figure:: img/right.png
   :alt: delete user

Project settings
^^^^^^^^^^^^^^^^ 

If you are admin on a project, the project settings button is enabled and, by clicking on it,
you will access the project setting page.

.. figure:: img/pr_settings.png
   :alt: project settings


On this page, you can :

* update name, description and color of your project
* delete the project. Please note that if you delete a project, all resources linked to the project will be deleted
(experiments, datasets, deployed models, etc.)
