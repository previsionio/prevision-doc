****************************
Versionning your experiments
****************************


In the Prevision.io platform you can iterate versions of your experiments.

All the version keep one common thing : the target of your training, that defines your experiment. Yet from one version to the others you can change any parameters to see how it impacts performance and stability of your model.


.. figure:: img/new_version.png
   :align: center
   :alt: The target stays the same and cannot be changed along versions of an experiment
   :scale: 50%

   The target stays the same and cannot be changed along versions of an experiment


To do that, three possibilities :

-  On the experiment list of a project, by clicking on the right side action button of the experiment you want to iterate and select “new version”
-  On any pages of a experiment by clicking on the top right “actions” button and select new version
-  On the “Version” menu of a experiment, by clicking on the action button right side of a version listed and select “new version”

Then, on the version menu of a experiment, you will find the list of all trained versions for this experiment. By clicking on the version number,
left side of this list, you will access the selected experiment version page. You can also navigate through versions by using the dropdown list
top left of the information banner on any page of a experiment.

.. figure:: img/image_46.png
   :alt: image alt text



After clicking on a new version button, you will be redirected to the
experiment configuration menu. The experiment version configuration you
selected for your iteration will be automatically loaded in order for
you to know what configuration was done and select the changement you
want to apply.


.. note::

   when creating a new experiment or a new version, add a description
   to your experiment at the first screen of new experiment configuration. It
   will help you finding the version you want to work with later.