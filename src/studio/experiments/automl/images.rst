*********************
Computer Vision Model
*********************

In the prevision.io platform, you can train models using images for :

- regression
- classification ( either simple one with 2 class or multiclassification with more than 2 class )

And a Special one :

- image detection


.. figure:: img/cv/nexxp.png
   :align: center
   :alt: Selecting an image use case
   :figclass: align-center
   :scale: 60%

   When selecting an image usecase, you can train a classification, a multiclassification, a regression or an object detector




Note that your models can be  trained on mixed dataset : you can have a dataset with tabular datas, columns of text, categorical  datas and linear one.

For example, this dataset is valid :

.. csv-table:: A dataset with mixed data
   :file: datas/mixed-dataset.csv
   :header-rows: 1

Setup
=====

When training an image use case, you always need 2 assets :

- a csv dataset with path to the images and differents info ( and the target )
- a zip of images uploaded in :doc:`your image folder storage space </studio/datas>`

The csv dataset is a classic dataset imported with connectors or uploaded via interface while the zip is a zip file of image set up on your local environment and uploaded to your image folder storage.

More over, for Object Detector traning, you need to draw bounding box onto your image and export them to Pascal Voc format. 

Regression
==========

A regression on images is a model that predict a number from an image, for example number of click from a thumbnail or a screenshot.

Classification
==============

A classification is a model that predict class of an image. There could be 2 class or more. For example "smiling", "crying" or "thinking" from the image of a face.


.. figure:: img/cv/classif.png
   :align: center
   :alt: Classif usecase with image datapath
   :figclass: align-center
   :scale: 60%

   A classification use case with Image path input 

Object detection
================

An object-detection use case is a model that from an image return zero, one or more bounding box with label.

.. figure:: img/cv/od.png
   :align: center
   :alt: Selecting an image use case
   :figclass: align-center
   :scale: 60%

   An image with its training bounding box ( *blue* ), the predicted bounding box ( *orange* )  with predicted label and probability





Image Path
==========

When there are image in a dataset, the path **to  each image** from **the root of the image folder** should be put in a column and this column muste then be selected as the "Image path" column.

For example if your folder of image has this architecture :


.. figure:: img/cv/tree.png
   :align: center
   :alt: 3 folders with images of cheese
   :figclass: align-center
   :scale: 80%

 
You should zip it from the root :


.. figure:: img/cv/zip.png
   :align: center
   :alt: zip from the root
   :figclass: align-center
   :scale: 80%

Upload the zip to your image folder Storage


.. figure:: img/cv/upload.png
   :align: center
   :alt: zip upload
   :figclass: align-center
   :scale: 80%


and your dataset should have a column with the following info

.. csv-table:: path info are from the root
   :file: datas/imgdataset.csv
   :header-rows: 1


When setting the experiment, this path from the zip root should be mapped to the 'image path' input.

Label or class
===============

For classification, Multiclassification and Object Detection, you must set a column with the name of a class, or label 


.. csv-table:: A label column
   :file: datas/classif.csv
   :header-rows: 1

You can call it anything you want but when setting your experiment, you need to select it in the 'class' input selector 

Bounding box
============

If you want to train a model to detect object on a image, you must run an Object Detection training and you **must provide** a dataset with bounding box for training


.. figure:: img/cv/params.png
   :align: center
   :alt: Object detection params
   :figclass: align-center
   :scale: 80%



This bounding box is described in a dataset in csv format that can be  :doc:`imported or uploaded to your data section</studio/datas>`. Bounding box always use 4 columns to be described but there are two format for bounding box :

- Pascal Voc bounding box : x-top left, y-top left,x-bottom right, y-bottom right
- Yolov bounding box : x-top left, y-top left, width, height ( *of the bounding box* )

Prevision Platform use the Pascal Voc Bounding box format in csv. So to describe a bounding box , you must provide 4 more columns and map them in the input interface when setting your object detection experiment :

.. csv-table:: A complete object detector dataset
   :file: datas/od_train.csv
   :header-rows: 1


Note that for object detection, there can be several object in one image. So your dataset may have many row with same image paht but different bounding box and label :

.. csv-table:: Several bounding box on one image
   :file: datas/bbb.csv
   :header-rows: 1


Object Segmentation
===================

Prevision.io does not provide Object Segmentation models. 


Launch train
============

Once you had mapped each of the input to the corresponding columns, and given a name to your experiment, you can click on the train button and the training will start.

For Regression, Classification and multiclassification, you will find the standard metrics and chart. For Object Detection, the metrics is Mape and represent the accuracy of the bounding box.


