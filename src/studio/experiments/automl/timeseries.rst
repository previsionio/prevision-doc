***********
Time series
***********


In the prevision.io platform you have the possibility to train time series experiment in order to do forecasting predictions. By selecting in the new experiment screen the timeseries data type you will access the timeseries experiment configuration.

.. figure:: img/slected_ts.png
   :alt: image alt text

Timeserie experiment configuration
==================================

Time series is very similar to tabular experiment except:

* There is no hold out
* There is no weight
* There is no fold (in this case, Prevision.io use temporal stratification)

However, you will find some new notions:

* Temporal column: the feature that contains the time reference of the time series. Since date formats can be complex, Prevision.io supports ISO 8601 (https://fr.wikipedia. org/wiki/ISO_8601) as well as standard formats (e.g. DD/MM/YYYY or DD-MM-YYYY hh:mm).
* Time step: period between 2 events (within the same group) from the temporal column (automatically detected)
* Observation window: illustrate the period in the past that you have for each prediction
  * Start of observation window: the maximum time step multiple in the past that you’ll have data from for each prediction (inclusive, 30 by default)
  * End of the observation window: the last time step multiple in the past that you’ll have data from for each prediction (inclusive, 0 by default that means that the immediate values before the prediction time step is known)
* Prediction window: illustrate the period in the future that you want to predict
  * Start of the prediction window: the first time step multiple you want to predict (inclusive, 1 by default which means we will predict starting at the next value)
  * End of the prediction window: the last time stamp multiple you want to predict (inclusive, 10 by default which means we will predict up to the 10th next value)
* A priori features: features whose value is known in the future (customer number, calendar, public holidays, weather…)
* Group features: features that identify a unique time serie (e.g. you want to predict your sales by store and by product. If you have 2 stores selling 3 products, there are 6 time series in your file. Selecting features « store » and « product in the group column allows Prevision.io to take into account these multiple series)


.. figure:: img/ts_fields.png
   :alt: image alt text

Please note that advanced options work the same way than for tabular experiments. Please read the corresponding readthedoc section in order to configure your time series experiment.
