***********
Predictions
***********

The predictions menu allows you to do bulk predictions using a previously loaded :doc:`dataset </studio/datas/>` and see holdout predictions made during training.

.. figure:: img/predictions.png
   :align: center
   :alt: Make and see predictions
   :scale: 50%

   Make and see predictions



In order to do a new prediction, you have to first select a model from the dedicated dropdown list and then a dataset uploaded on the project. Then, by clicking on the “launch prediction” button, the system will compute and generate a prediction file downloadable by clicking on the right side button on the prediction list below.

