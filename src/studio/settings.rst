********
Settings
********

Most of your settings are in the profile section, from the upper-right menu

.. figure:: img/settings/setting_location.png
   :align: center
   :alt: Profile Section is un the upper right menu
   :figclass: align-center
   :scale: 60%


   Profile Section is un the upper right menu


Here you can find you master Token ("API")  and you security information for authentication with 3rd party service.


Connecting my code
==================

For a lot of Prevision feature ( components, app deployment,...) you may connect your git repo, either Gitlab or github.

This takes place in the Federated identity tab :


.. figure:: img/settings/fi.png
   :align: center
   :alt: Connecting GIT repo
   :figclass: align-center
   :scale: 60%


   Connecting GIT repo


.. admonition:: Account and repositories

   Github and gitlab offer both OAuth2 authentication and git repo hosting. the *github* and *gitlab* fields are for OAuth2 service, the *github repositories* and *gitlab repositories* are for connecting your code repo.


Just click on the Add button and enter your credentials. Your code repo will now be available in the :doc:`Components </studio/pipelines>` section and :doc:`Deployments </studio/deployments/index>` section of the Prevision Studio.


