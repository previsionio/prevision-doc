Navigation
----------

Several levels of navigation are displayed on the interface in order for
you to navigate into the different projects and resources created on the
platform.

The first level of navigation is the left navigation bar. You will find
the following menu :

.. figure:: img/navigation.png
   :alt: Navigation



-  Studio : list of all available projects
-  Notebooks : access to the notebooks environment
-  Help : documentation and examples of prevision.IO possibilities
-  Store : opening a new tab to the prevision.io’s store


.. note::
   Tips : you can collapse/expand the left menu by clicking on the arrow button next to prevision studio logo on top of the menu

This menu is contextual, once you will get into a project, resources
available for this project will be reachable thanks to this main
navigation menu.

By clicking on the top right user icon, a sub-navigation will appear

.. figure::img/subnavigation.png
   :alt: subnavigation



-  Language : switch between french and english version
-  Profile : access your personal profile information
-  Administration & API key : access the API key configuration and the
   admin screen (if you have the admin rights)
-  Documentation : access to platform documentation
-  Terms and conditions : display terms and conditions of Prevision.io
   platform
-  Log out : log out the platform
