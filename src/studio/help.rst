Help
====

By clicking on help on the main menu, you will be redirected to the prevision’s ressources helping you through the application and experiments. Four sections are available :

* type of problem : helping you to define what kind of experiment type is the most suitable for your issue
* videos : centralisation of tutorials & data-science ressources
* medium post : our datascience deticated posts published on medium
* documentation : link to the application documentation such as the readthedoc or the SDK documentation
