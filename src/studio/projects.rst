********
Projects
********


In Prevision.io studio, ressources, such as datasets or models, are scoped by project in order to structure your work and collaborate easily with people inside a project.

A project is a collection of :

- :doc:`Datas <datas>` : for importing and exporting your data from and to external database or files
- :doc:`Experiments <experiments/index>` : to build model, evaluate them and compare them
- :doc:`Pipelines <pipelines>` : to set up and schedule datascience pipelines 
- :doc:`Deployments <deployments/index>` : to push models to production and monitor it
- :doc:`Collaborators <collaborators>` : to add and magage users

List my Projects
================

All your projects are available on the homepage of your server. You can reach it with the mosaic icon on the upper-left corner of each screen

.. figure:: img/list_projects.png
   :align: center
   :alt: features
   :figclass: align-center
   :scale: 50%


   Go to the list of projects


You can switch from card view

.. figure:: img/card_view.png
   :align: center
   :alt: Projects
   :figclass: align-center
   :scale: 50%


   List of projects


To List view :

.. figure:: img/projects/list.png
   :align: center
   :alt: features
   :figclass: align-center
   :scale: 50%


   List of projects


You will find the following information on the cards :

-  project name
-  created by and creating at
-  description (if available)
-  number of datasets/pipelines/use cases
-  list of collaborators into the project and their associated role into this project

If your role has been setted up as admin into a project, an action
button on top right of each card will be available. By clicking on this
button you will be able to edit the project information and delete the
project. Please note that deleting a project will also delete all sub
project’s items, such as pipelines or datasets, created on the project.

.. note::
   Tips : you can filter projects by their names using the search bar on
   top right of the projects view

Create a new project
====================
All your datascience project assets belong to a project. In order to Experiment, import models, deploy or monitor you need to create a new project.

When viewing the list of project on `your homepage <https://cloud.prevision.io/projects/list>`_, you can create a new project, by clicking the “new project” button on top right of the “my projects” view.

You will access to the following interface :

.. figure:: img/projects/create.png
   :align: center
   :alt: create a project
   :figclass: align-center
   :scale: 50%


   Create a Project



In order to create your project you have to fulfill at least a color and a project name. You can also add a description of your project.

Please note that you can at any moment, if admin role into a project has been setted up for your account, change these information by clicking on “settings” into the project menu.

All your projects will be displayed in the “my projects” view. Two different displays are list view and cards view and you can switch between one view and another by clicking on the view button you prefere next to the search bar.



Project navigation
==================


By entering a project, you will first be redirected to the project homepage. The following sections, including the 3 latest entries for each section, are displayed :

* Datasets : last uploaded dataset
* Pipelines : last pipeline templates
* Usecases : last experiments

Under each section you will also find a link to the dedicated page, also available through the left project main menu, and, for pipelines and experiment, a shortcut to create new ones.

Into a project you can load data, create a pipeline in order to automate some task or data transformations and train models into use cases menu.
Once you enter a project by clicking on its card or on the list, the project menu will be loaded on the left navigation bar. 


.. figure:: img/project-nav.png
   :alt: image alt text

- Home : you will find here last items from datasets, pipelines and use cases created into the project.
- Usecases : you will find all your use cases trained into the selected project
- Data : you will find here your datasets and the connectors setted up on the project
- Pipelines : you will be able thanks to pipeline to augment your data and automate some actions
- Deployment : deploy and monitor deployed models and applications
- Collaborators : list of all project collaborators and their associated project role
- Settings : if your project role is admin, this menu is available and allows you to edit the project informations


collaboration into a project
----------------------------

Prevision.io studio is built in order for our users to collaborate within the projects. To do that, into the “collaborators” menu of a selected project you can manage, if your role is admin on the project, the collaborators and the right inside the project.

.. figure:: img/project-collaboration.png
   :alt: image alt text


* Add a user : by enter the email address of a Prevision.io platform registered user you can add a collaborator
* role : if your role level is admin into the project you will be able to edit user roles
* by clicking the delete button on the right side of a user, you can disable the access to the selected user 
  to   the project


Project roles
-------------


Into a project there are 3 levels of roles :

* End-user : in this project, the user can only access to the list of deployed models and applications and make predictions
* Viewer : you can navigate into all ressources of a project and visualize information but you can’t download or create ressources
* Contributor : Viewer rights + you can create and manage resources of the project
* Admin : Contributor rights + you can manage project users and project settings


Edit a project
==============

You can change the following parameters of your project by clicking on settings on the main navigation of your project or, on the list/card view of your project by clicking on the action button.

* Name of the project
* Description of the project
* Color displayed on the card of the project


Delete a project
================

If a project is no longer useful, you can delete it by clicking on the action button on the card/list projects view.

Warning : all ressources created into the project will be deleted with the suppression of the project with no possibility of back-up. If deleted, a project and its resources are no longer available for you but also for all users previously added to this project.


