******************************
Prevision.io --- Documentation
******************************


Value proposition
=================

`Prevision.io <https://cloud.prevision.io>`_ brings powerful AI management capabilities to data science users so more AI projects make it into production and stay in production. Our purpose-built AI Management platform was designed by data scientists for data scientists and citizen data scientists to scale their value, domain expertise, and impact. The platform manages the hidden complexities and burdensome tasks that get in the way of realizing the tremendous productivity and performance gains AI can deliver across your business

Requirements
============

Prevision.IA is a SAAS platform optimized for Firefox and Chrome navigators.
The cloud version can be accessed online at `<https://cloud.prevision.io>`_,
or it can be deployed on-premise or in your private cloud.
Please visit us at `<https://prevision.io>`_ if you have any questions regarding our deployment capabilities.

Conditions
==========
Please read the general terms and conditions available on following link : https://cloud.prevision.io/terms



contacts
========
If you have any questions about using Prevision.io platform please contact us using the chat button on the Prevision.io
store interface or by email at the following contact address :

support@prevision.io



Getting started
===============

There are to ways to get a Prevision.io account :

#. Go to https://cloud.prevision.io and open an account ( or link one of your OAuth2 enabled account )
#. Deploy a dedicated computing service on Google Cloud

In order to deploy Prevision.io on Google Cloud, go to the :doc:`dedicated Guide <./gcp>`


Account creation
----------------

By clicking to the following address, you will land on the connection page which allows you to create an account or
sign in if you already have a Prevision.io account.

https://cloud.prevision.io

In order to create a new account, just click on the sign up button next to the log in button.
You will access the following account creation form.

.. figure:: img/signup.png
   :align: center
   :alt: features
   :figclass: align-center
   :scale: 50%


   Signup screen


Once you have filled the needed information, you will have a 30 days free but limited access to the Prevision.io platform.
In order to upgrade your free trial account into a full access one, please contact us on following email (support@prevision.io)

Once done you can follow our :doc:`complete guide to release a model </howto/datascience/release_a_model>` 

Connection
----------

Once your account has been created, you will be able to access the
prevision’s Studio and Store and start creating models and deploying
them. 

Please note that SSO using you google/linkedin/GitHub account is
available.

.. figure:: img/image_1.png
   :align: center
   :alt: features
   :figclass: align-center
   :scale: 50%


   login with SSO





Cloud & freetrial limitations
-----------------------------

If you are using our `cloud platform <https://cloud.prevision.io>`_  using a free trial account, some limitations are set up. Here's a quick view of limitations for free accounts:

.. list-table:: Freetrial Limitation
   :header-rows: 1

   * - Entity
     - Action 
     - Limitation
   * - PROJECT
     - Create Project
     - Free trial users can create 2 Limited Projects
   * - DATASETS
     - Add dataset from file / datasource
     - 10 Datasets max + 1GB per dataset
   * - IMAGE FOLDER
     - Add / Update / Delete image Folder in project
     - 1 Image Folder
   * - DATA SOURCE 
     - Add / Update / Delete datasource in project 
     - 1 Datasource max
   * - CONNECTOR
     - Add / Update / Delete connector in project
     - 1 Connector max
   * - USECASE
     - Add / Update / Delete experiment in project
     - 5 Use Cases max
   * - USECASE VERSION
     - Add / Update experiment version in project
     - 3 Concurrent experiment versions
   * - PREDICTION
     -  Add / Update prediction in experiment version
     - 2 Concurrent predictions
   * - STORE APP
     - Deploy Apps
     - 5 Concurrent deployed apps

     

    

.. toctree::
   :maxdepth: 4
   :caption: Reference Documentation

   studio/index
   API/index
   SDK/index
   howto/index