
*************************************
Google Cloud Marketplace subscription
*************************************

Prevision.io is available form Google Cloud Marketplace. If you already have a Google Cloud account, you can deploy Prevision Platform on Google Cloud and use the pay as you go model. 

Pre-requisites
==============


To deploy Prevision on Google Cloud you must already have a `service account <https://cloud.google.com/iam/docs/service-accounts>`_  and a `billing account enabled <https://console.cloud.google.com/billing>`_ .

Steps
=====

1. Go to the `google Market Place <https://console.cloud.google.com/marketplace>`_ page and search for Prevision.io

.. figure:: img/gcp/img_1.png
   :align: center
   :alt: Google Marketplace
   :scale: 60%

   Google Marketplace Homepage

2. Or go to `the Prevision install page <https://console.cloud.google.com/marketplace/product/prevision-public/prevision.io>`_ 

.. figure:: img/gcp/img_4.png
   :align: center
   :alt: Prevision io setup page
   :scale: 60%

   Prevision io setup page

3. Click on the Subscribe button. You  will be asked to link your billing account or create one. Be advised than creating a billing account often needs 2 to 3 days for google to validate your billing informations.

.. figure:: img/gcp/img_6.png
   :align: center
   :alt: Link your billing account
   :scale: 60%

   Link your billing account

4.  Once you got a billing account, and before validating, check the pricing. Prevision.io is a pay as you go pricing scheme meaning that you only pay when you use the system 

.. figure:: img/gcp/img_7.png
   :align: center
   :alt: Pricing
   :scale: 50%

   Pricing

5. Once you agree to the terms, you will be redirected to the Prevision.io subscription page. Create a username and a password, enter the required informations and submit. 


.. figure:: img/gcp/img_9.png
   :align: center
   :alt: Opening an account
   :scale: 60%

   Opening an account

6. An instance will be created. **The process takes about 10mn** and you will get an email once your instance is created.

.. figure:: img/gcp/img_10.png
   :align: center
   :alt: Instance deployed
   :scale: 60%

   Instance deploying

7. Once you received your email, click on the provided link to go to your instance. First time you logged you will be met with Discover screen and tutorial

.. figure:: img/gcp/img_11.png
   :align: center
   :alt: Pricing
   :scale: 50%

   Discover screen

You can now start to :doc:`import your data </studio/datas>`, :doc:`prepare your features </studio/pipelines>`, :doc:`train your model </studio/experiments/automl/index>` or :doc:`import existing models </studio/experiments/external-model>`
