##################################
Using the Prevision Quantum NN SDK
##################################

Prevision-quantum-nn is a library that allows to handle automatically quantum variational circuits. Its main page is available `here <https://prevision-quantum-nn.readthedocs.io/en/latest/index.html>`_.

.. toctree::
    :maxdepth: 2

    Getting started <https://prevision-quantum-nn.readthedocs.io/en/latest/quickstart.html>
    API reference <https://prevision-quantum-nn.readthedocs.io/en/latest/autoapi/prevision_quantum_nn/index.html>
    Source code <https://github.com/previsionio/prevision-quantum>
    Blog post <https://medium.com/prevision-io/prevision-quantum-nn-automating-quantum-neural-networks-part-1-3-d19b86154c64>
