****************************************************
Standard Machine Learning Worflow with Prevision SDK
****************************************************

Here is a list of standard and common workflows you can acheive with Prevision Python  SDK. You may read the `Prevision Public github <https://github.com/previsionio/prevision-onnx-templates>`_ or the `API reference <https://prevision-python.readthedocs.io/en/latest/source/api.html>`_ too.



.. toctree::
    :maxdepth: 2

    external-model
