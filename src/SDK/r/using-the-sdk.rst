Say goodbye to the ML data-prep and AI model deployment headaches!

For years now, machine learning and data science have been a growing trend that more and more people took an interest in. This dynamic duo is an amazing opportunity to incorporate the incredible amount of data we gather each second into our solutions to everyday problems. On the other hand, be it because it’s your job or because you are just a happy enthusiast, you might have noticed that when doing a machine learning project, an important part of it consists in repeating the same steps over and over again: finding datasets, preprocessing them, extracting relevant features from them, rewriting the same wrapper around a data science lib model to fit in your project…
As the domain developed, the idea of automating some of this process grew as well. This is why nowadays, the concept of auto-ML (for “automated machine learning”) is spreading like wildfire. In addition to making the life of data scientists with a math or programming background easier, it is also a way of bringing more people to the party and welcoming what we call “citizens data scientists”. As explained by Gartner, this term refers to people that use data science tools and solve real-life problems thanks to it without having a deep formation in stats or analytics.
So, what if instead of building, training and optimizing yet another model, you could delegate this tiresome task to a robust and efficient system to focus on actually interpreting the results and thinking about what it means? What if instead of wasting time on exhausting and resource-consuming small steps you could forget about these repetitive tasks, generate accurate models easily and then toy around with them to do actual research on your problem? And what if everyone could take part in this grand adventure without having to worry about the underlying technical complexity?
Prevision.io: a powerful and easy-to-use auto-ML solution
Prevision.io’s platform is a complete ML project lifecycle software that relies on auto-ML and is meant to help you create efficient AI models without having to code anything. It is a no-code tool that has been designed to let anyone try their luck with data science: whereas a senior data scientist will be pleased to find customization and advanced analytics for their ML pipelines, a citizen data scientists or an amateur can simply enjoy the ride and deploy lots of models to test out their ideas.
The whole goal of the tool is to give you an efficient and well-prepared framework that hides away all the storage, analysis or deployment complexity.
Prevision.io is user-friendly and offers an online web version with an intuitive UI that makes it easy to start taking advantage of all the features and the power of the platform. On the other hand, if you’re like me, you like buttons and all, but you enjoy scripts even more!
So in this article, I will show how to use Prevision.io’s R SDK to do a basic classification task.

For the ones that prefer to use Python for their data science projects, Prevision.io also provides a Python SDK that can be directly cloned from their github and install as a small package. My colleague Zeineb Ghrib recently did a nice article teaching you the basics of this Python SDK.
Setting up the R SDK
Installing the R SDK from Prevision.io’s repository
Prevision.io’s R SDK has a few dependencies that should be installed beforehand using R usual command install.packages (in a R console):
> install.packages(c("data.table", "futile.logger", "httr", "jsonlite"))
Then, to download and install Prevision.io’s R SDK, you need to clone the Prevision.io R SDK repository, to build the package and to install it:
git clone https://bitbucket.org/previsionio/prevision-r
R CMD build prevision-r
R CMD INSTALL previsionio_<SDK_VERSION>.tar.gz
where the `<SDK_VERSION>` depends on the current downloadable version of the SDK and follows the usual version format major.minor.patch. For example, at the time of this writing, I built a `previsionio_10.3.0.tar.gz` file.
Retrieving your master token
Be it through the web interface or an SDK, the use of Prevision.io’s solution requires you to have an account on a specific instance. To create an account, go to Prevision.io’s website and click on the “Test us” button. Once you have this account, you can connect to the web interface and retrieve your master token that will be necessary for the upcoming configuration steps.
Here is how to get your token:
connect to your Prevision.io instance online
go to the menu bar at the top and, on the far right, click on your user initials
in the popup menu, select “Administration & API key”

finally, in the “Master token” block on the right, generate new key if you haven’t already and click on the “copy” label to have it in your clipboard

There you go: you now have your master token and you can use it to run a script with Prevision.io’s SDK!
Connecting to Prevision.io’s platform
Whenever you want your code to access your Prevision.io instance, you need to do a small initialization step at the very beginning to ensure you are properly connected to the platform.
After importing the SDK package, you simply need to create a new client by passing in your master token and the address of your instance to the SDK method:
library(previsionio)
pioUrl = "https://<INSTANCE_NAME>.prevision.io"
pioToken = "<TOKEN>"
# initialize the connection
initPrevisionioClient(pioToken, pioUrl)
Note: remember to replace the `<TOKEN>` variable in this code block with your own master token and the `<INSTANCE_NAME>` with the name of your own Prevision.io instance.
How to do a basic classification?
Since Zeineb’s article shows us how to do a regression on the California Housing dataset, let’s switch gears here and instead work on a binary classification problem. We will work on the well-known Titanic dataset and, as usual, try and predict the survival chances.
For the ones that want to dive in the code head first, here is the R notebook for this tutorial.
Importing our dataset from the Prevision.io platform
Suppose we already uploaded the full dataset to the platform and want to retrieve it for our use case. We can easily do so using Prevision.io’s SDK by getting the id of the dataset through its name, and then getting the dataframe itself:
datasetId = getDatasetIdFromName(“titanic.csv”)
dataset = createDataframeFromDataset(datasetId)
This dataset object is a Prevision Dataset object that holds some metadata on your data (such as a unique id…) as well as the actual dataframe:

Transforming the dataset
Now that we have our data, we can explore and modify it for our liking. For example, we can add or remove some features from the dataset. Here, we draw the new information from the one we currently have as a simple demo: we group the age values into age groups and we compute the square root of the fare:
ageGroups = c(0, 25, 50, 75)
dataset$AgeGroup = cut(dataset$Age, ageGroups, labels=FALSE)
dataset$SqrtFare = sqrt(dataset$Fare)
Doing the train/test split
As is very often the case in a machine learning project, we need to divide our data into two parts: the train and the test sets. By making sure no information leaks from one to the other, we mitigate the risk of having our model “learn-by-heart” the right answer and instead make for a much better machine learning experience.
We will separate with a classic split ratio of 80/20: 80% of our initial data will be taken for the training subset and 20% for the testing subset:
nSamples = nrow(titanic)
splitRatio = 0.8
trainIds = sample(seq_len(nSamples), size=floor(splitRatio * nSamples))
train = titanic[trainIds,]
test = titanic[-trainIds,]
Thanks to Prevision.io’s SDK, it is just as easy to reupload our newly splitted datasets to the platform:
dsTrain = createDatasetFromDataframe(datasetName = ‘titanic_train’,
dataframe = train)
dsTest = createDatasetFromDataframe(datasetName = ‘titanic_test’,
dataframe = test)
We are now ready to launch the actual training!
Running a use case
To run a use case and train models on your training subset, you just have to call the `startUsecase()` function and pass it some configuration parameters, then it will automatically take care of starting everything on the platform for you.
Configuring the use case columns
In order for the platform to know what your training target is, or whether you have some specific id columns that should not be taken into account during computation, you need to specify some “column configuration” for your use case.
These columns are arguments of the `startUsecase()` function and there are 5 interesting parameters:
`targetColumn`: the name of the target column in the dataset
`idColumn` (optional): the name of an id column that has no value for the model (it doesn’t have any true signal) but is just a handy list of references for example; this column should thus be ignored during training (but it will eventually be rematched to the prediction sample to give you back the full data)
`foldColumn` (optional): if you want to perform a custom stratification to improve the quality of your predictions (which is sometimes better than regular cross-validation, as shown by Kohavi), you can pass a specific column name to use as reference; if none is provided, a random stratification will be used and will try to force the same distribution of the target between folds
`weightColumn` (optional): sometimes, a numerical does not contain an actual feature but rather an indication of how important each row is — if that is the case, you can pass the name of this column as `weightColumn` (the higher the weight, the more important the row — by default, all rows are considered to be of equal importance); note that if this is provided, the optimised metric will become weighted
`dropList` (optional): you can pass a list of column names that you wish to exclude from the training (they will simply be ignored)
Configuring the training profile
You can also fine-tune your use case options by configuring a training profile. This ensemble of variables will decide several things for your use case: what models are tested out, what metric is used, the desired types of feature engineering…
The function offers you a range of options to choose from, among which some that are used quite often:
`models`: the list of “full” models you want to add to your training pipeline chosen among “LR”, “RF”, “ET”, “XGB”, “LGB” and “NN”
`simpleModels`: the list of “simple” models you want to add to your training pipeline chosen among “LR” and “DT”
`featuresEngineeringSelectedList`: the list of feature engineering blocks to integrate in the pipeline (these will be applied on your dataset during training to extract relevant information and transform it in the best possible way for the models fit step)
`profile`: this Prevision.io specific is a way of setting a global run mode that determines both training time and performance. You may choose between 3 profiles:
the “quick” profile runs very fast but has a lower performance (it is recommended for early trials)
the “advanced” profile runs slower but has increased performance (it is usually for optimization steps at the end of your project)
the “normal” profile is something in-between to help you investigate an interesting result
`withBlend`: if you turn this setting on, you will allow Prevision.io’s training pipeline to append additional “blend” models at the end that are based on some cherry-picked already-trained models and proceed to further optimization to usually get even better results
A key feature of Prevision.io that is demonstrated here, with the fact that we pass lists of models, is that one Prevision.io use case actually bundles multiple models together. As opposed to a by-hand data science analysis where you usually write down two or three models at most for comparison, the platform’s engine is able to train multiple models in parallel and compare their efficiency on your dataset. Thanks to this feature, you directly get a feeling of the best model type for your use case.
Starting the use case!
To create the use case and start your training session, simply call the `startUsecase()` method with the configuration you prepared:
uc = startUsecase(name = "titanic_example",
                  dataType = "tabular",
                  trainingType = "classification",
                  datasetId = dsTrain$`_id`,
                  targetColumn = "Survived",
                  idColumn = "PassengerId",
                  profile = "quick",
                  models = c("XGB"),
                  simpleModels = c("LR", "DT"))
This function starts the use case and returns a new Prevision Usecase object as soon as it has started. Be careful: the training is asynchronous which means that even though the method has finished executing and you have your use case object, the actual training is not done yet!
If you want to examine the current status of your use case, you can go on your instance web interface or use the `getUsecaseInfos()` function to extract the status parameter:
infos = getUsecaseInfos(uc$experimentId)
print(infos$status)
Since we just started the training, this should return “running” for now. Depending on the complexity of the use case (the size of the training dataset, the number of models, the additional profile options…) it may take from a few minutes to a few hours for the training to finish.
When it is done, if you get the information on your use case again and re-examine the status parameter, you’ll see it now has a “done” value.
Asking for a prediction on our test set
After our use case has finished training at least one model, we can ask the platform to make a prediction on a specific dataset using one of the available models. Earlier, we uploaded a test dataset for our Titanic example. We can therefore launch a prediction based on the current use case object and the test subset by calling the `startPrediction()` function:
pred = startPrediction(uc$experimentId, dsTest$`_id`)
This function creates a new Prevision Prediction object that holds various properties. In particular, it has a unique id that can be used to retrieve the results of the prediction when it’s done computing (depending on the size of your test set and the model you chose, it might take a while — here, it should be finished after a few seconds):
prediction = getPrediction(uc$experimentId, pred$`_id`)
print(prediction)

As expected, we see that for each line in our test dataset, we have:
a prediction for the class (either 0 or 1, in the `Survived` column)
the predicted probability associated with it (in the `pred_Survived` column)
the matching row from the id column (the `PassengerId` column) that was not used during training but got rematched in the end to each sample
Getting more info on a use case and its models using the SDK
Once you have trained your use case, you can access various information about it such as the list of all trained models, the best one, the fastest one…
To get this metadata on the use case, you can use the `getUsecaseInfos()` method once again or the `getUsecaseModels()` function:
infos = getUsecaseInfos(uc$experimentId)
models = getUsecaseModels(uc$experimentId)
print(infos)
print(models)
This will give you a whole set of parameters to examine that include (but are not limited to): the list of versions for your use case, the exact moment it started and ended, the metric used during training, the number and the full list of trained models, various scores…
If you look more closely at the list of models (gathered via the `getUsecaseModels()` call), you’ll see that Prevision.io tags models with several interesting keys to help you decide which one is the most suitable for your situation:
the “best” model is the one that gave you the best performance according to the metric you chose (for example: the highest AUC or the lowest log loss)
the “fastest” model is the one that gives you results in the shortest time (this predict time was evaluated during the training phase by trying out predictions on a small batch of your data)
the “simple” models are specifically designed to be easy to understand: if you go on the web interface, you can even get Python, R and Mysql representations of their prediction process (as small human-readable scripts or commands)
What else can you do with Prevision.io’s platform?
Prevision.io’s tool can be used in many instances and offers enough profiles or configuration settings to serve both during your development test phase and in production. Because it is quick and easy to use, even non-specialists of the data science domain can have a go.
The platform lets you run use cases for several types of datasets: tabular, timeseries or image folders; and for up to 4 types of problems, depending on the data type: regressions, binary classifications, multi-classifications and object detection. All in all, it allows you to treat plenty of business cases, from electrical consumption to financial analysis, churn prediction, market segmentation…
The big advantage of using Prevision is that the tool automatically trains and compares several models on your experiment. Rather than having to prepare each by hand, you simply run your use case and you can then analyze the results of each by browsing the analysis screens on the platform.
Each model can be studied in detail to better understand what went well or not and why it was considered relevant by the platform:

Note: here we retrieve the same information as we did through the SDK before and in particular we get the score the model had on the validation dataset and its predict response time — this can help us choose the best or the fastest model.
You can also see the features relative importance, get various metrics on your models (such as accuracy, precision, recall…) and study other charts like the confusion matrix for a classification:



In addition to all this, the platform incorporates a nice data analysis tool, too. As explained in another recent article by my colleague Arnold Zephir, Prevision.io’s integrates a data exploring tool that lets you take a peek at your data and get a feeling of salient points.
You can also run notebooks in R or in Python directly on the platform to play around with the datasets you uploaded.
Finally, let’s say you are more of a citizen data scientist who works in team with an expert data scientist. Thanks to the platform, it is possible for your colleague to setup and fine-tune a model in the Prevision.io “studio” and, with just the click of a button, to deploy it as an online application in the Prevision.io “store”. This application can now be accessed directly with your account credentials and you can get predictions from the trained model on any input you want without having to worry about training or deployment issues!
Conclusion
I truly believe machine learning is to be one of the most widely and prominent tools in the years to come. It has already been implemented in lots of places — some of which we may not be aware of — and as the potential to revolutionize our relationship to data.
However, we cannot leave it in the hands of just a happy few. I’m convinced that, on this topic, we should all understand some basics so that we are conscious of the possible outcomes, that we are not oblivious to the problems and that we don’t shy away from the risks. Auto-ML platforms like Prevision.io make it possible for everyone who is interested to take a peek at this fascinating domain and if you’re already a pro, it could take care of all the nasty tasks for you, so feel free to dive in!