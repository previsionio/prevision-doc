###############
Using the R SDK
###############

- `Getting started <https://medium.com/prevision-io/using-prevision-ios-auto-ml-platform-r-sdk-91ef13913f01>`_
- :download:`API reference  <datas/sdkr_doc.pdf>`
- `Source code <https://github.com/previsionio/prevision-r>`_
